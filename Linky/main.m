//
//  main.m
//  Linky
//
//  Created by ch.yamuna on 25/02/16.
//  Copyright © 2016 YAMUNA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
