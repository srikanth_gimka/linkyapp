
//
//  ForgotPasswordViewController.m
//  Linky
//
//  Created by ch.yamuna on 25/02/16.
//  Copyright © 2016 YAMUNA. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController{
    UITextField*emailTextField;
    UIButton* submit;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden=NO;
    self.title=@"FORGOT PASSWORD";
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.view.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.navigationController.navigationBar.backgroundColor=[UIColor whiteColor];
    

    
    UIImage*image=[UIImage imageNamed:@"clear"];
    UIButton*barButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [barButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:barButton];
    [barButton addTarget:self action:@selector(performbackAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=leftBarButton;
    
    emailTextField=[[UITextField alloc]initWithFrame:CGRectMake(30, 200, 320, 40)];
    emailTextField.textColor=[UIColor blackColor];
    emailTextField.backgroundColor=[UIColor whiteColor];
    emailTextField.borderStyle=UITextBorderStyleNone;
    emailTextField.placeholder=@"Email";
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border.frame = CGRectMake(0, emailTextField.frame.size.height - borderWidth, emailTextField.frame.size.width, emailTextField.frame.size.height);
    border.borderWidth = borderWidth;
    [emailTextField.layer addSublayer:border];
    emailTextField.layer.masksToBounds = YES;
    [self.view addSubview:emailTextField];
    
    submit =[[UIButton alloc]initWithFrame:CGRectMake(50 ,260 , 280, 50)];
    [submit setFont:[UIFont boldSystemFontOfSize:16]];
    [submit setTitle:@" SUBMIT " forState:UIControlStateNormal];
    [submit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submit.backgroundColor=[UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f];
    [submit addTarget:self action:@selector(performSubmitAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submit];

    
}

-(void)performbackAction:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)performSubmitAction:(id)sender{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
