//
//  SignUpViewController.m
//  Linky
//
//  Created by ch.yamuna on 26/02/16.
//  Copyright © 2016 YAMUNA. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController{
    
    UITextField *fullNameField;
    UITextField * emailTextField;
    UITextField *phoneNumberField;
    UITextField *passwordField;
    UITextField *confirmPasswordField;
    UISegmentedControl *segmentedControl;
    UIButton *signUp;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden=NO;
    
    self.title=@" SIGN UP";
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.view.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    self.navigationController.navigationBar.clipsToBounds = YES;
    self.navigationController.navigationBar.backgroundColor=[UIColor whiteColor];

    
    UIImage*barImage=[UIImage imageNamed:@"clear"];
    UIButton*barButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [barButton setBackgroundImage:barImage forState:UIControlStateNormal];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:barButton];
    [barButton addTarget:self action:@selector(performbackAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=leftBarButton;

    UIImage*image1=[UIImage imageNamed:@"profile pic"];
    UIImageView*imageview= [[UIImageView alloc]initWithFrame:CGRectMake(115   , 70, 150, 150)];
    imageview.image=image1;
    [self.view addSubview:imageview];
    
    
    fullNameField=[[UITextField alloc]initWithFrame:CGRectMake(30, 240, 320, 40)];
    fullNameField.textColor=[UIColor blackColor];
    fullNameField.backgroundColor=[UIColor whiteColor];
    fullNameField.borderStyle=UITextBorderStyleNone;
    fullNameField.placeholder=@"FULL NAME";
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border.frame = CGRectMake(0, fullNameField.frame.size.height - borderWidth, fullNameField.frame.size.width, fullNameField.frame.size.height);
    border.borderWidth = borderWidth;
    [fullNameField.layer addSublayer:border];
    fullNameField.layer.masksToBounds = YES;
    [self.view addSubview:fullNameField];
    
    emailTextField=[[UITextField alloc]initWithFrame:CGRectMake(30, 300, 320, 40)];
    emailTextField.textColor=[UIColor blackColor];
    emailTextField.backgroundColor=[UIColor whiteColor];
    emailTextField.borderStyle=UITextBorderStyleNone;
    emailTextField.placeholder=@"EMAIL";
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 2;
    border1.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border1.frame = CGRectMake(0, emailTextField.frame.size.height - borderWidth1, emailTextField.frame.size.width, emailTextField.frame.size.height);
    border1.borderWidth = borderWidth1;
    [emailTextField.layer addSublayer:border1];
    emailTextField.layer.masksToBounds = YES;
    [self.view addSubview:emailTextField];

    
    phoneNumberField=[[UITextField alloc]initWithFrame:CGRectMake(30, 360, 320, 40)];
    phoneNumberField.textColor=[UIColor blackColor];
    phoneNumberField.backgroundColor=[UIColor whiteColor];
    phoneNumberField.placeholder=@"PHONE";
    phoneNumberField.borderStyle=UITextBorderStyleNone;
    phoneNumberField.secureTextEntry=YES;
    CALayer *border2 = [CALayer layer];
    CGFloat borderWidth2 = 2;
    border2.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border2.frame = CGRectMake(0, phoneNumberField.frame.size.height - borderWidth2, phoneNumberField.frame.size.width, phoneNumberField.frame.size.height);
    border2.borderWidth = borderWidth2;
    [phoneNumberField.layer addSublayer:border2];
    [self.view addSubview:phoneNumberField];
    
    passwordField=[[UITextField alloc]initWithFrame:CGRectMake(30, 420, 320, 40)];
    passwordField.textColor=[UIColor blackColor];
    passwordField.backgroundColor=[UIColor whiteColor];
    passwordField.placeholder=@"PASSWORD";
    passwordField.borderStyle=UITextBorderStyleNone;
    passwordField.secureTextEntry=YES;
    CALayer *border3 = [CALayer layer];
    CGFloat borderWidth3 = 2;
    border3.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border3.frame = CGRectMake(0, passwordField.frame.size.height - borderWidth3, passwordField.frame.size.width, passwordField.frame.size.height);
    border3.borderWidth = borderWidth3;
    [passwordField.layer addSublayer:border3];
    [self.view addSubview:passwordField];
    
    confirmPasswordField=[[UITextField alloc]initWithFrame:CGRectMake(30, 480, 320, 40)];
    confirmPasswordField.textColor=[UIColor blackColor];
    confirmPasswordField.backgroundColor=[UIColor whiteColor];
    confirmPasswordField.placeholder=@"CONFIRM PASSWORD";
    confirmPasswordField.borderStyle=UITextBorderStyleNone;
    confirmPasswordField.secureTextEntry=YES;
    CALayer *border4 = [CALayer layer];
    CGFloat borderWidth4 = 2;
    border4.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border4.frame = CGRectMake(0, confirmPasswordField.frame.size.height - borderWidth4, confirmPasswordField.frame.size.width, confirmPasswordField.frame.size.height);
    border4.borderWidth = borderWidth4;
    [confirmPasswordField.layer addSublayer:border4];
    [self.view addSubview:confirmPasswordField];

    NSMutableArray*genderNames=[[NSMutableArray alloc]initWithObjects:@"Male",@"Female", nil ];
    segmentedControl = [[UISegmentedControl alloc] initWithItems:genderNames];
    segmentedControl.frame = CGRectMake(110, 535, 160, 35);
    //    segmentedControl.selectedSegmentIndex=0;
    [segmentedControl setTintColor:[UIColor purpleColor]];
    segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    [segmentedControl addTarget:self action:@selector(segmentGenderAction:) forControlEvents: UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];

    
    signUp =[[UIButton alloc]initWithFrame:CGRectMake(50 ,580 , 280, 50)];
    [signUp setFont:[UIFont boldSystemFontOfSize:16]];
    [signUp setTitle:@" SIGN UP " forState:UIControlStateNormal];
    [signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    signUp.backgroundColor=[UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f];
    [signUp addTarget:self action:@selector(performSignUpAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signUp];

    
}




-(void)performbackAction:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)segmentGenderAction:(id)sender{
    if (segmentedControl.selectedSegmentIndex==0) {
        
        
    }
    else{
        
    }
}
-(void)performSignUpAction:(id)sender{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
