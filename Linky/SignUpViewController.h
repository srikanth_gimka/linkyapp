//
//  SignUpViewController.h
//  Linky
//
//  Created by ch.yamuna on 26/02/16.
//  Copyright © 2016 YAMUNA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
