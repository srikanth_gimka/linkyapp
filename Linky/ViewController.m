//
//  ViewController.m
//  Linky
//
//  Created by ch.yamuna on 25/02/16.
//  Copyright © 2016 YAMUNA. All rights reserved.
//

#import "ViewController.h"
#import "ForgotPasswordViewController.h"
#import "SignUpViewController.h"
@interface ViewController ()

@end

@implementation ViewController{
    UITextField* emailTextField;
    UITextField * passwordTextField;
    UIButton *signInButton;
    UIButton *forgottenButton;
    UILabel *accountLabel;
    UIButton* signUpButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden=YES;
    UIImage*image=[UIImage imageNamed:@"linky logo"];
    UIImageView*imaweView= [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+125,self.view.frame.origin.y+150, self.view.frame.size.width-250, 150)];
    imaweView.image=image;
    [self.view addSubview:imaweView];
    
    
    emailTextField=[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+30,self.view.frame.origin.y+430, 320, 40)];
    emailTextField.textColor=[UIColor blackColor];
    emailTextField.backgroundColor=[UIColor whiteColor];
    emailTextField.borderStyle=UITextBorderStyleNone;
    emailTextField.placeholder=@"Email";
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border.frame = CGRectMake(0, emailTextField.frame.size.height - borderWidth, emailTextField.frame.size.width, emailTextField.frame.size.height);
    border.borderWidth = borderWidth;
    [emailTextField.layer addSublayer:border];
    emailTextField.layer.masksToBounds = YES;
    [self.view addSubview:emailTextField];
    

    
    passwordTextField=[[UITextField alloc]initWithFrame:CGRectMake(30, 490, 320, 40)];
    passwordTextField.textColor=[UIColor blackColor];
    passwordTextField.backgroundColor=[UIColor whiteColor];
    passwordTextField.placeholder=@"Password";
    passwordTextField.borderStyle=UITextBorderStyleNone;
    passwordTextField.secureTextEntry=YES;
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 2;
    border1.borderColor = [UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f].CGColor;
    border1.frame = CGRectMake(0, passwordTextField.frame.size.height - borderWidth1, passwordTextField.frame.size.width, passwordTextField.frame.size.height);
    border1.borderWidth = borderWidth1;
    [passwordTextField.layer addSublayer:border1];
    [self.view addSubview:passwordTextField];
    
    
   forgottenButton =[[UIButton alloc]initWithFrame:CGRectMake(5 ,5, 180, 25)];
    [forgottenButton setFont:[UIFont systemFontOfSize:16]];
    [forgottenButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [forgottenButton setTitle:@"Forgotten password?" forState:UIControlStateNormal];
        [passwordTextField setRightViewMode:UITextFieldViewModeAlways];
    [forgottenButton addTarget:self action:@selector(performForgotPasswordAction:) forControlEvents:UIControlEventTouchUpInside];
    [passwordTextField setRightView:forgottenButton];


    
   signInButton =[[UIButton alloc]initWithFrame:CGRectMake(50 ,550 , 280, 50)];
    [signInButton setFont:[UIFont boldSystemFontOfSize:16]];
    [signInButton setTitle:@" SIGN IN " forState:UIControlStateNormal];
    [signInButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    signInButton.backgroundColor=[UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f];
    [signInButton addTarget:self action:@selector(performSignInAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signInButton];
    
    
    accountLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 615, 220, 20)];
    [accountLabel setFont:[UIFont systemFontOfSize:14]];
    accountLabel.text=@"DON'T HAVE ACCOUNT ?";
    accountLabel.textColor=[UIColor blackColor];
    [self.view addSubview:accountLabel];

    signUpButton =[[UIButton alloc]initWithFrame:CGRectMake(220 ,615 , 90, 20)];
    [signUpButton setFont:[UIFont systemFontOfSize:16]];
    [signUpButton setTitle:@" SIGN UP " forState:UIControlStateNormal];
    [signUpButton setTitleColor:[UIColor colorWithRed:136/255.0f green:0/255.0f blue:160/255.0f alpha:1.0f] forState:UIControlStateNormal];
   
    [signUpButton addTarget:self action:@selector(performSignUpAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signUpButton];
    
}
-(void)performForgotPasswordAction: (id)sender {

    ForgotPasswordViewController*fpvc=[[ForgotPasswordViewController alloc]init];
    [self.navigationController pushViewController:fpvc animated:YES];

}
-(void)performSignUpAction: (id)sender {
    SignUpViewController*suvc=[[SignUpViewController alloc]init];
    [self.navigationController pushViewController:suvc animated:YES];

}
-(void)performSignInAction:(id)sender{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
