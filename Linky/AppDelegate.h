//
//  AppDelegate.h
//  Linky
//
//  Created by ch.yamuna on 25/02/16.
//  Copyright © 2016 YAMUNA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

